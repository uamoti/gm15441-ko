#!/bin/bash

QCDIR=/data/home/bernardomso/VDS2/Kornfeld/bernardo/QC/
MQC_OUT=/data/home/bernardomso/VDS2/Kornfeld/bernardo/multiqc/

if [ ! -d $MQC_OUT ]; then
    mkdir $MQC_OUT
fi

multiqc $QCDIR -o $MQC_OUT

