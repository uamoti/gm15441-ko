#!/bin/bash

FQ=/data/home/bernardomso/VDS2/NGS_Runs/190319_A00316_0061_AH77VJDRXX/Data/Intensities/BaseCalls/BMSO_JWK/
QC_OUT=/data/home/bernardomso/VDS2/Kornfeld/bernardo/QC/

if[ ! -d $QC_OUT ]; then
    mkdir $QC_OUT
fi

for file in $(ls $FQ); do
    fastqc -o $QC_OUT --extract $file
done

