#!/bin/bash

FQ=/data/Kornfeld/bernardo/190319_A00316_0061_AH77VJDRXX/cat_fq/
SAM=/data/Kornfeld/bernardo/190319_A00316_0061_AH77VJDRXX/BAM/
STAR_INDEXES=/data/Genomes/mouse/mm10/star/index/
STAR=/data/bin/star/STAR_2.4.2a/bin/Linux_x86_64/STAR
GTF=/data/Genomes/mouse/mm10/mm10.Ens79.gtf
OUT_DIR=/data/Kornfeld/bernardo/190319_A00316_0061_AH77VJDRXX/star_workflow/sorted/

if [ ! -d $OUT_DIR ]; then
    mkdir $OUT_DIR
fi

cd $FQ

for prefix in $(ls | cut -d _ -f 1 | uniq); do
    R1=$prefix'_R1'.fq.gz
    R2=$prefix'_R2'.fq.gz
    
    $STAR --runThreadN 8 --genomeDir $STAR_INDEXES --sjdbGTFfile $GTF --readFilesCommand zcat --readFilesIn $R1 $R2 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix $OUT_DIR$prefix
done

