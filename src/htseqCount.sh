#!/bin/bash

DIR=/data/Kornfeld/bernardo/190319_A00316_0061_AH77VJDRXX/star_workflow/
BAM=$DIR/sorted/
GTF=/data/Genomes/mouse/mm10/mm10.Ens79.gtf

htseq-count -q -f bam -r pos -s no $(ls $BAM/*.bam) $GTF > hts_counts.tsv

