#!/bin/bash

FQ=/storage/Kornfeld/bernardo/FQ/
MERGEDIR=/storage/Kornfeld/bernardo/merged/

if [ ! -d $MERGEDIR ]; then
    mkdir $MERGEDIR
fi

cd $FQ

for prefix in $(ls *.gz | cut -d "_" -f -1 | uniq); do
    cat $(ls $prefix*_R1_*.gz) > $MERGEDIR/$prefix\_R1.fq.gz
    cat $(ls $prefix*_R2_*.gz) > $MERGEDIR/$prefix\_R2.fq.gz
done

rm *_R1*
rm *_R2*

