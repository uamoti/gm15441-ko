#!/bin/bash

SAM=/storage/Kornfeld/bernardo/mapped/
BAM=/storage/Kornfeld/bernardo/sorted/

if [ ! -d $BAM ]; then
    mkdir $BAM
fi

cd $SAM

for file in $(ls); do
    name=$(basename $file .sam)
    samtools view -u $file | samtools sort -@ 6 -o $BAM$name.bam -
done

