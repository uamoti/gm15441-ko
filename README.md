# RNA-seq of Gm15441 knock-out livers

Male mice of about 16 weeks of age were fed *ad libitum* and fasted for 24 h before sacrifice.

This project was conducted at University of Southern Denmark (SDU - Odense), Kornfeld lab of lncRNAs, FGM unit, between January 2019 and February 2020.
